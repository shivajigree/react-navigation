/** @format */

import {AppRegistry} from 'react-native'
import {createStackNavigator, createAppContainer} from 'react-navigation'
// import Main from './Component/MainComponent';
import {name as appName} from './app.json';

//Components
import MainComponent from'./Component/MainComponent';
import SecondComponent from './Component/SecondComponent';
import ThirdComponent from './Component/ThirdComponent';

//Screen Names
import { MainScreen, SecondScreen, ThirdScreen } from './screenNames';

const AppNavigator = createStackNavigator({
    MainScreen:{
        screen: MainComponent,
        navigationOptions: {
            headerTitle: 'International Mountain Museum',

        },
    },
    SecondScreen: {
        screen: SecondComponent,
        navigationOptions: {
            headerTitle: 'Second',
            headerTintColor: '#fff',
            headerStyle: {
                backgroundColor: '#6a5acd',
            },
        },
    },
    ThirdScreen: {
        screen: ThirdComponent,
        navigationOptions: {
            headerTitle: 'Fetched Data',
            headerTintColor: '#fff',
            headerStyle: {
                backgroundColor: '#9acd32',
            },
        },
    }
},{
    initialRouteName: MainScreen
});

const AppContainer = createAppContainer(AppNavigator);

AppRegistry.registerComponent(appName, () => AppContainer);
