import React, { Component } from "react";
import { Text, View, Image, Button } from "react-native";
import {
  createBottomTabNavigator,
  createDrawerNavigator,
  createAppContainer
} from "react-navigation";

class FirstView extends Component {
  static navigationOptions = {
    tabBarLabel: "First",
    tabBarIcon: ({ tintColor }) => (
      <Image
        source={{ uri: "https://img.icons8.com/metro/104/000000/home.png" }}
        style={{ width: 26, height: 26, tintColor: tintColor }}
      />
    )
  };
  render() {
    return (
      <View
        style={{
          flex: 1,
          backgroundColor: "red",
          justifyContent: "center",
          alignItems: "center"
        }}
      >
        <Text style={{ color: "#fff", fontSize: 24 }}>First View is here</Text>
      </View>
    );
  }
}

class SecondView extends Component {
  static navigationOptions = {
    tabBarLabel: "Second",
    tabBarIcon: ({ tintColor }) => (
      <Image
        source={{ uri: "https://img.icons8.com/metro/104/000000/settings.png" }}
        style={{ width: 26, height: 26, tintColor: tintColor }}
      />
    )
  };
  render() {
    return (
      <View
        style={{
          flex: 1,
          backgroundColor: "mediumseagreen",
          alignItems: "center",
          justifyContent: "center"
        }}
      >
        <Text style={{ color: "#fff", fontSize: 24 }}>Second View is here</Text>
        <View style={{ height: 50 }} />
        <Button
          style={{}}
          onPress={() => {
            this.props.navigation.navigate("SecondScreen");
          }}
          title="Goto SecondScreen"
        />
        <View style={{ height: 50 }} />
        <Button
          style={{}}
          onPress={() => {
            this.props.navigation.navigate("ThirdScreen");
          }}
          title="Goto ThirdScreen to fetch data"
        />
      </View>
    );
  }
}

class ThirdView extends Component {
  static navigationOptions = {
    tabBarLabel: "Third",
    tabBarIcon: ({ tintColor }) => (
      <Image
        source={{ uri: "https://img.icons8.com/metro/104/000000/phone.png" }}
        style={{ width: 26, height: 26, tintColor: tintColor }}
      />
    )
  };
  render() {
    return (
      <View
        style={{
          flex: 1,
          backgroundColor: "blue",
          alignItems: "center",
          justifyContent: "center"
        }}
      >
        <Text style={{ color: "#fff", fontSize: 24 }}>Third View is here</Text>
      </View>
    );
  }
}

const TabNavigator = createBottomTabNavigator(
  {
    First: FirstView,
    Second: SecondView,
    Third: ThirdView
  },
  {
    order: ["First", "Second", "Third"],
    animationEnabled: true
  },
  {
    tabBarOptions: {
      activeTintColor: "#7567B1",
      labelStyle: {
        fontSize: 16,
        fontWeight: "600"
      }
    }
  }
);

const DrawerNavigator = createDrawerNavigator(
  {
    First: FirstView,
    Second: SecondView,
    Third: ThirdView
  },
  {
    tabBarOptions: {
      activeTintColor: "#7567B1",
      labelStyle: {
        fontSize: 16,
        fontWeight: "600"
      }
    }
  }
);

export default createAppContainer(DrawerNavigator && TabNavigator);
