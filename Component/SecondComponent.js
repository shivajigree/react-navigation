import React, { Component } from "react";
import { Text, View, Button, ImageBackground } from "react-native";
// import Button from 'react-native-button';
import { MainScreen, ThirdScreen } from "../screenNames";

export default class MainComponent extends Component {
  render() {
    return (
      <View
        style={{
          flex: 1,
          backgroundColor: "dodgerblue"
        }}
      >
        {/* Banner start*/}
        <View
          style={{
            height: "35%",
            backgroundColor: "#555",
            justifyContent: "center",
            alignItems: "center"
          }}
        >
          <ImageBackground
            source={{
              uri:
                "https://www.hongkongfp.com/wp-content/uploads/2016/11/1067615.jpg"
            }}
            resizeMode="cover"
            // blurRadius={2}
            style={{ width: "100%", height: "100%" }}
          />
        </View>
        {/* Banner end */}

        <Text style={{ fontWeight: "bold", fontSize: 22, color: "white" }}>
          This is the main screen
        </Text>

        <View
          style={{
            // flex: 1,
            flexDirection: "row",
            justifyContent: "center",
            alignItems: "center"
          }}
        >
          <Button
            onPress={() => {
              this.props.navigation.navigate(MainScreen);
            }}
            title="  Trekking"
          />
          <Button
            style={{}}
            onPress={() => {
              this.props.navigation.navigate(ThirdScreen);
            }}
            title="Explore Pokhara"
          />
        </View>
      </View>
    );
  }
}
