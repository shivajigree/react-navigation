import React, { Component } from "react";
import {
  Alert,
  Text,
  View,
  StyleSheet,
  FlatList,
  Platform,
  ActivityIndicator, ImageBackground
} from "react-native";

export default class ThirdComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: true
    };
  }

  componentDidMount() {
    return fetch("http://musicalnepal.cu.ma/softee/List.php")
      .then(response => response.json())
      .then(responseJson => {
        this.setState(
          {
            isLoading: false,
            dataSource: responseJson
          },
          function() {
            // In this block you can do something with new state.
          }
        );
      })
      .catch(error => {
        console.error(error);
      });
  }

  FlatListItemSeparator = () => {
    return (
      <View
        style={{
          height: 1,
          width: "100%",
          backgroundColor: "#607D8B"
        }}
      />
    );
  };

  GetFlatListItem(place_name) {
    Alert.alert(place_name);
  }

  render() {
    if (this.state.isLoading) {
      return (
        <View style={{ flex: 1, paddingTop: 20 }}>
          <ActivityIndicator />
        </View>
      );
    }

    return (
      <View style={styles.MainContainer}>
        <FlatList
          data={this.state.dataSource}
          ItemSeparatorComponent={this.FlatListItemSeparator}
          renderItem={({ item }) => (
            <View style={styles.FlatListItemStyle}> 
              <ImageBackground source={{uri: item.place_image}} style={{width: '100%', height: '100%'}}>
                  <View style={{position: 'absolute', top: 0, left: 0, right: 0, bottom: 0, justifyContent: 'flex-start', alignItems: 'flex-start'}}>
                  <Text
                      style={{fontSize: 22, color: '#3ef'}}
                      onPress={this.GetFlatListItem.bind(this, item.place_name)}
                    >
                      {" "}
                      {item.place_name}{" "}
                    </Text>
                  </View>
              </ImageBackground>
           
            </View>
          )}
          keyExtractor={(item, index) => index}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  MainContainer: {
    justifyContent: "center",
    flex: 1,
    paddingBottom: 10,
    paddingTop: Platform.OS === "ios" ? 20 : 0
  },

  FlatListItemStyle: {
    
    height: 150
  }
});
