var gridViewItems = [
        {
          key: "Mustang",
          color: "#40e0d0",
          imageUrl:
            "https://i.ytimg.com/vi/TVBHQpHY8u4/maxresdefault.jpg"
        },
        {
          key: "Manang",
          color: "#6a5acd",
          imageUrl:
            "https://i.ytimg.com/vi/ERNTxDjjpjs/maxresdefault.jpg"
        },
        {
          key: "Tilicho Lake",
          color: "green",
          imageUrl:
            "https://i.ytimg.com/vi/1qrKR2Jn_AI/maxresdefault.jpg"
        },
        {
          key: "Annapurna Circuit Trek",
          color: "#9acd32",
          imageUrl:
            "https://www.insidehimalayas.com/wp-content/uploads/2017/09/KVKO3080-1.jpg"
        }
      ]
export default gridViewItems;